<?php

namespace App\Models\Backend;

use App\Model;

class OperateLog extends Model
{
    protected $table = 'operate_log';
    protected $guarded = ['id'];
}
